package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"

	"aurt.sun/app"
	"aurt.sun/config"
	. "github.com/logrusorgru/aurora"
)

// vars
var packageCount = 0
var completedCount = 0
var failedCount = 0

func main() {

	// when app exit will it remove temp dir.
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		<-c
		app.RemoveAurtTempDir()
		os.Exit(1)
	}()

	// Load config file.
	_ = config.InitConfig("./")

	// Get flag for set password.
	passwd := flag.String("p", "", "set password for used install package with 'pacman'.")

	// Get flag for search.
	findPackage := flag.String("s", "", "search package, set option '--a' for search package from .pkgs file.")

	// Get flag for install package by commandline.
	packageByCommandline := flag.String("i", "", "set package for search and install package by commandline.")

	// Get flag for test aurt app.
	useTest := flag.Bool("test", false, "unit test project.")

	// Parse Flag to var
	flag.Parse()

	// check if devmode is true.
	if config.AurtConfig.AppDevmode {
		// unit test project
		if *useTest {
			fmt.Printf("<------ [ %s : %s ] ------>\n", Bold(Blue("TESTING")), Bold(Green("Load config")))

			/// noting to do.

			fmt.Printf("<------ [ %s : %s ] ------>\n", Bold(Blue("Completed")), Bold(Green("Load config")))

			os.Exit(0)
		}
	}

	// Set shell to use.
	app.UseShell(config.AurtConfig.AppConfig.UseShell)

	// Set link to download a snapshot of the package source files.
	app.UseSnapshotPackageSourceFile(config.AurtConfig.AppConfig.SourcePackage)

	// Set ftp to download a package files.
	app.UseFTPPackageFile(config.AurtConfig.PkgTarXz.PkgTarXzHost, config.AurtConfig.PkgTarXz.PkgTarXzPort, config.AurtConfig.PkgTarXz.PkgTarXzUser, config.AurtConfig.PkgTarXz.PkgTarXzPass, config.AurtConfig.PkgTarXz.PkgTarXzPath)

	// Search one package.
	if *findPackage != "" && *findPackage != "--a" {
		seachOnePackageCase(*findPackage)
	}

	// Search list package.
	if *findPackage == "--a" {
		seachPackageListCase()
	}

	// Check password input
	if *passwd == "" {
		log.Fatalf("%s : %s", Bold(Red("[error]")), "Not have '-p' for set password.")
	}

	// Set password for install pacman -U package pkg.tar.xz.
	app.UsePassword(*passwd)

	// Check if input package name on commandline will install package by this.
	if *packageByCommandline != "" {
		installOnePackageCase(*packageByCommandline)
	}

	// Install package list from '.pkgs' file.
	installPackageListCase()

}

func installPackageListCase() {

	// Get package name from pkg file.
	fileNameList, err := app.ReadFileFromFilePackage(config.AurtConfig.AppConfig.PkgsFile)
	if err != nil {
		log.Fatalf("%s : %s", Bold(Red("[error]")), err)
	}

	// Check list is empty
	if len(fileNameList) == 0 {
		log.Fatalf("%s : %s", Bold(Red("[error]")), "Package list in pkg file is empty.")
	}

	// Init install package from 'fileNameList'
	for _, fileName := range fileNameList {
		packageCount++

		fmt.Println("")

		fmt.Printf("<------ [ (%d) | %s : %s ] ------>\n", Bold(Blue(packageCount)), Bold(Green("Installing")), Bold(Green(fileName)))
		status, err := app.InitInstallPackage(fileName)
		if err != nil {
			log.Fatalf("%s : %s", Bold(Red("[error]")), err)
		}

		if status == 1 {
			completedCount++
			fmt.Printf("<------ [ (%d) | %s : %s ] ------>\n", Bold(Blue(packageCount)), Bold(Green("Completed")), Bold(Green(fileName)))
		} else {
			failedCount++
			fmt.Printf("<------ [ (%d) | %s : %s ] ------>\n", Bold(Blue(packageCount)), Bold(Red("Failed")), Bold(Red(fileName)))
		}
	}

	// Show summary install.
	app.ShowSummaryInstallPackage(packageCount, completedCount, failedCount)

	// Remove temp dir
	app.RemoveAurtTempDir()
}

func installOnePackageCase(packageByCommandline string) {
	packageCount = 1

	fileName := packageByCommandline

	fmt.Println("")

	fmt.Printf("<------ [ (%d) | %s : %s ] ------>\n", Bold(Blue(packageCount)), Bold(Green("Installing")), Bold(Green(fileName)))
	status, err := app.InitInstallPackage(fileName)
	if err != nil {
		log.Fatalf("%s : %s", Bold(Red("[error]")), err)
	}

	if status == 1 {
		completedCount++
		fmt.Printf("<------ [ (%d) | %s : %s ] ------>\n", Bold(Blue(packageCount)), Bold(Green("Completed")), Bold(Green(fileName)))
	} else {
		failedCount++
		fmt.Printf("<------ [ (%d) | %s : %s ] ------>\n", Bold(Blue(packageCount)), Bold(Red("Failed")), Bold(Red(fileName)))
	}

	// Show summary install."")
	app.ShowSummaryInstallPackage(packageCount, completedCount, failedCount)

	// Remove temp dir
	app.RemoveAurtTempDir()

	// exit app
	os.Exit(0)
}

func seachPackageListCase() {

	// Get package name from pkg file.
	fileNameList, err := app.ReadFileFromFilePackage(config.AurtConfig.AppConfig.PkgsFile)
	if err != nil {
		log.Fatalf("%s : %s", Bold(Red("[error]")), err)
	}

	// Check list is empty
	if len(fileNameList) == 0 {
		log.Fatalf("%s : %s", Bold(Red("[error]")), "Package list in pkg file is empty.")
	}

	// Init seach package from 'fileNameList'
	for _, fileName := range fileNameList {
		packageCount++

		fmt.Println("")

		fmt.Printf("<------ [ (%d) | %s : %s ] ------>\n", Bold(Blue(packageCount)), Bold(Green("Search")), Bold(Green(fileName)))
		status, err := app.InitSearchPackage(fileName)
		if err != nil {
			log.Fatalf("%s : %s", Bold(Red("[error]")), err)
		}

		if status == 1 {
			completedCount++
			fmt.Printf("<------ [ (%d) | %s : %s ] ------>\n", Bold(Blue(packageCount)), Bold(Green("Found")), Bold(Green(fileName)))
		} else {
			failedCount++
			fmt.Printf("<------ [ (%d) | %s : %s ] ------>\n", Bold(Blue(packageCount)), Bold(Red("Missing")), Bold(Red(fileName)))
		}
	}

	// Show summary search.
	app.ShowSummarySearchPackage(packageCount, completedCount, failedCount)

	// exit app
	os.Exit(0)
}

func seachOnePackageCase(packageName string) {
	packageCount = 1

	fmt.Printf("<------ [ (%d) | %s : %s ] ------>\n", Bold(Blue(packageCount)), Bold(Green("Searching")), Bold(Green(packageName)))
	status, err := app.InitSearchPackage(packageName)
	if err != nil {
		log.Fatalf("%s : %s", Bold(Red("[error]")), err)
	}

	if status == 1 {
		completedCount++
		fmt.Printf("<------ [ (%d) | %s : %s ] ------>\n", Bold(Blue(packageCount)), Bold(Green("Found")), Bold(Green(packageName)))
	} else {
		failedCount++
		fmt.Printf("<------ [ (%d) | %s : %s ] ------>\n", Bold(Blue(packageCount)), Bold(Red("Missing")), Bold(Red(packageName)))
	}

	// Show summary search.
	app.ShowSummarySearchPackage(packageCount, completedCount, failedCount)

	// exit app
	os.Exit(0)
}
