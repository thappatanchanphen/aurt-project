module aurt.sun

go 1.12

require (
	github.com/janeczku/go-spinner v0.0.0-20150530144529-cf8ef1d64394
	github.com/logrusorgru/aurora v0.0.0-20190803045625-94edacc10f9b
	github.com/stretchr/testify v1.4.0 // indirect
	github.com/tj/go-spin v1.1.0
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4 // indirect
	gopkg.in/dutchcoders/goftp.v1 v1.0.0-20170301105846-ed59a591ce14
	gopkg.in/yaml.v2 v2.2.2
)
