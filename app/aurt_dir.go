package app

import (
	"os"
)

// * For check it has 'directory'.
// return true,false
func checkHasDir(directoryPath string) bool {
	_, err := os.Stat(directoryPath)
	if err != nil {
		return false
	}
	return true
}

// * Create aurt 'temp dir', when exit app 'temp dir' will has delete.
func createAurtTempDir() error {
	err := makeDirAll(getAurtTempDir())
	if err != nil {
		return err
	}
	//defer os.RemoveAll(getAurtTempDir())
	return nil
}

// * For make 'directory'.
func makeDirAll(path string) error {
	err := os.Mkdir(path, 0700)

	if err == nil || os.IsExist(err) {
		return nil
	}

	return err
}

// * Get aurt temp 'directory'.
func getAurtTempDir() string {
	tmpDir := os.TempDir()
	aurtDir := tmpDir + "/aurt-build"

	return aurtDir
}

// * Remove aurt temp directory
func RemoveAurtTempDir() string {
	err, _, outputErr := execCommand("rm -rf " + getAurtTempDir())
	if err != nil {
		return outputErr + " " + err.Error()
	}

	return ""
}
