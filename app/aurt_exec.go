package app

import (
	"bytes"
	"errors"
	"os/exec"
)

var shellUsed = "bash"
var passwdUsed = ""

// * Exec command .
func execCommand(command string) (error, string, string) {
	var stdout bytes.Buffer
	var stderr bytes.Buffer
	cmd := exec.Command(shellUsed, "-c", command)
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	return err, stdout.String(), stderr.String()
}

// * Set used shell for exec (default is 'sh').
func UseShell(shellInput string) {
	if shellInput != "" {
		shellUsed = shellInput
	}
}

// * Set password for exec
func UsePassword(passwdInput string) {
	if passwdInput != "" {
		passwdUsed = passwdInput
	}
}

// * Get password for exec
func getPassword() (string, error) {
	if passwdUsed == "" {
		return "", errors.New("Password has empty")
	}

	return passwdUsed, nil
}
