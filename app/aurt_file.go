package app

import (
	"bufio"
	"bytes"
	"errors"
	"io"
	"net/http"
	"os"
	"path"
	"strings"
	"time"
)

const filePathDefault = ".pkgs"
const backupFilePathDefault = ".baks"

// * open '.pkgs' file for reading. If successful, methods on
// the returned fileNameList from .pkgs files.
// descriptor if filePath has empty. default will set to '.pkgs' path.
// If there is an error, it will be of type Error.
func ReadFileFromFilePackage(filePath string) ([]string, error) {
	// vars.
	var pakageNameList []string
	pathFilePkg := ""

	// set path file.
	pathFilePkg = filePath

	// check has filePath from arg.
	if pathFilePkg == "" {
		pathFilePkg = filePathDefault
	}

	file, err := os.Open(pathFilePkg)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		if scanner.Text() != "" {
			if strings.TrimSpace(string(scanner.Text()[0])) != "#" {
				pakageNameList = append(pakageNameList, strings.TrimSpace(scanner.Text()))
			}
		}
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return pakageNameList, nil
}

// * open '.baks' file for reading. If successful, methods on
// the returned dirsList from .baks files.
// descriptor if filePath has empty. default will set to '.baks' path.
// If there is an error, it will be of type Error.
func ReadDirFromFileBks(filePath string) ([]string, error) {
	// vars.
	var backupDirList []string
	pathFileBaks := ""

	// set path file.
	pathFileBaks = filePath

	// check has filePath from arg.
	if pathFileBaks == "" {
		pathFileBaks = backupFilePathDefault
	}

	file, err := os.Open(pathFileBaks)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		if scanner.Text() != "" {
			if strings.TrimSpace(string(scanner.Text()[0])) != "#" {
				backupDirList = append(backupDirList, strings.TrimSpace(scanner.Text()))
			}
		}
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return backupDirList, nil
}

// * Download file from url.
func DownloadFile(packageName string, url string, dest string) (time.Duration, error) {

	file := path.Base(url)

	var path bytes.Buffer
	path.WriteString(dest)
	path.WriteString("/")
	path.WriteString(file)

	start := time.Now()

	out, err := os.Create(path.String())

	if err != nil {
		return 0, err
	}

	defer out.Close()

	headResp, err := http.Head(url)

	if err != nil {
		return 0, err
	}

	defer headResp.Body.Close()

	resp, err := http.Get(url)

	if err != nil {
		return 0, err
	}

	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return 0, errors.New(resp.Status)
	}

	_, err = io.Copy(out, resp.Body)

	if err != nil {
		return 0, err
	}

	elapsed := time.Since(start)

	return elapsed, nil
}

// * Search file from url.
func SearchFile(url string) error {

	headResp, err := http.Head(url)

	if err != nil {
		return err
	}

	defer headResp.Body.Close()

	resp, err := http.Get(url)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return errors.New(resp.Status)
	}

	if err != nil {
		return err
	}

	return nil
}

// * Untar file from path.
func UnTarGz(fileName string, path string, toPath string) (time.Duration, error) {
	start := time.Now()

	err, _, errOut := execCommand("tar xvf " + path + "/" + fileName + ".tar.gz" + " -C " + toPath)
	if err != nil {
		return 0, errors.New(errOut)
	}

	elapsed := time.Since(start)

	return elapsed, nil
}

// * Build file from path.
func BuildFile(fileName string, path string) (time.Duration, error) {
	start := time.Now()

	err, _, errOut := execCommand("cd " + path + "/" + fileName + " && makepkg -s --noconfirm sudo")
	if err != nil {
		return 0, errors.New(errOut)
	}

	elapsed := time.Since(start)

	return elapsed, nil
}

// * Install file from path.
func InstallFile(fileName string, path string) (time.Duration, error) {
	start := time.Now()

	packageTarXz, errorGetPackage := getInstallFilePackage(fileName, path)
	if errorGetPackage != nil {

		return 0, errorGetPackage
	}

	if packageTarXz == "" {
		return 0, errors.New("Not found package (.pkg.tar.xz)")
	}

	errIntsall := InstallPackageFromPkgTarFile(packageTarXz, path+"/"+fileName)
	if errIntsall != nil {
		return 0, errIntsall
	}

	elapsed := time.Since(start)

	return elapsed, nil
}

// * Install pkg.tar.xz file.
func InstallPackageFromPkgTarFile(pkgTarFileName string, dir string) error {
	passwd, errPass := getPassword()
	if errPass != nil {
		return errPass
	}

	err, _, errOut := execCommand("cd " + dir + " && echo " + passwd + " | sudo -S pacman --noconfirm -U " + pkgTarFileName)
	if err != nil {
		return errors.New(errOut)
	}

	return nil
}

// * Get Install File name from temp (.pkg.tar.xz)
func getInstallFilePackage(packageName string, path string) (string, error) {
	err, output, errOut := execCommand("cd " + path + "/" + packageName + " && ls | grep .pkg.tar.xz")
	if err != nil {
		return "", errors.New(errOut)
	}

	return output, nil

}

// * Remove file from path
func RemoveFile(fileName string, path string) {
	execCommand("rm -f " + path + "/" + fileName)
}
