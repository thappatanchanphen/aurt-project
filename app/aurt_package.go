package app

import (
	"errors"
	"fmt"
	"time"

	. "github.com/logrusorgru/aurora"
)

var urlSourcePackage = "aur.archlinux.org/cgit/aur.git/snapshot/" //$pkg.tar.gz

// * Search package with package name.
func InitSearchPackage(packageName string) (int, error) {

	// search package from ftp.
	_, errSearchFTPState := seachPackageFromFTP(packageName)
	if errSearchFTPState != 1 {

		// search package from source.
		errSearchSourceState := seachPackageFromSource(packageName)
		if errSearchSourceState != 1 {
			return 0, nil
		}

	}

	return 1, nil
}

// * Install package with package name.
func InitInstallPackage(packageName string) (int, error) {
	// check aurt dir, if not has dir will create new aurt temp dir.
	if !checkHasDir(getAurtTempDir()) {
		err := createAurtTempDir()
		if err != nil {
			return 1, err
		}
	}

	//# FTP : process.
	//		- find package from url `https://ftp.lysator.liu.se/pub/manjaro/stable/community/x86_64/`,
	//		  if has package use this package for installed.
	//		- ftp to ftp.lysator.liu.se
	//		- user : anonymous
	//		- pass : anonymous
	//		- nlist {package name}*

	//# URL : process.
	//		- wget "https://{urlSourcePackage}packageName.tar.gz" # Download a snapshot of the package source files
	//	 	- tar xvf "$pkg.tar.gz" Decompress i
	//	 	- cd "packageName"
	//	 	- makepkg -si # -s stands for build, -i stands for install

	// install from ftp file.
	stateInstallFTP := stateInstallFromFTP(packageName)
	if stateInstallFTP != 1 {

		// install from source file.
		stateInstallSourceFile := stateInstallFromSourceFile(packageName)
		if stateInstallSourceFile != 1 {
			return 0, nil
		}

	}

	return 1, nil
}

// * install with snapshot of the package source files (source file).
func stateInstallFromSourceFile(packageName string) int {

	// Search snapshot of the package source files
	statusSearch := seachPackageFromSource(packageName)
	if statusSearch != 1 {
		return 0
	}

	// Download a snapshot of the package source files
	statusDownload := downloadPackage(packageName)
	if statusDownload != 1 {
		return 0
	}

	// Untar a package file to tmp
	statusUntar := untarPackage(packageName)
	if statusUntar != 1 {
		return 0
	}

	// Build package in tmp dir.
	statusBuild := buildPackage(packageName)
	if statusBuild != 1 {
		return 0
	}

	// Install package in tmp dir.
	statusInstall := installPackage(packageName)
	if statusInstall != 1 {
		return 0
	}

	return 1
}

// * install with ftp (.pkg.tar.xz).
func stateInstallFromFTP(packageName string) int {

	// find package fron ftp
	packageData, errState := seachPackageFromFTP(packageName)
	if errState != 1 {
		return 0
	}

	// download package from ftp.
	dirTmpPackage, packageName, statusDownload := downloadPackageFromFTP(packageName, packageData)
	if statusDownload != 1 {
		return 0
	}

	// install package from ftp.
	statusInstall := installPackageFromFTP(packageName, dirTmpPackage)
	if statusInstall != 1 {
		return 0
	}

	return 1

}

// * Set link download a snapshot of the package source files (default is 'aur.archlinux.org/cgit/aur.git/snapshot/').
func UseSnapshotPackageSourceFile(urlSourcePackageInput string) {
	if urlSourcePackageInput != "" {
		urlSourcePackage = urlSourcePackageInput
	}
}

// * Download package from ftp.
func downloadPackageFromFTP(packageNameInstall string, packageData string) (string, string, int) {
	spin := SpinnerShow("Dowloading ")

	downloadTiming, dirTmpPackage, packageFileTar, err := DownloadPackageFromFTP(packageData, packageNameInstall)

	spin.Stop()

	if err != nil {
		SpinnerErrorMessage("Downloading", err)
		RemoveFile(packageFileTar, dirTmpPackage)
		return "", "", 0
	}

	SpinnerCompletedMessage("Downloading", downloadTiming)

	return dirTmpPackage, packageFileTar, 1
}

// * Install package from ftp.
func installPackageFromFTP(packageFileTar string, dirTmpPackage string) int {
	start := time.Now()

	spin := SpinnerShow("Installing  ")

	err := InstallPackageFromPkgTarFile(packageFileTar, dirTmpPackage)

	elapsed := time.Since(start)

	spin.Stop()

	if err != nil {
		SpinnerErrorMessage("Installing ", err)
		return 0
	}

	SpinnerCompletedMessage("Installing ", elapsed)

	return 1
}

// * Download package from package name.
func downloadPackage(packageName string) int {
	packageNameTarGz := packageName + ".tar.gz"
	urlDownloadPackage := "https://" + urlSourcePackage + packageNameTarGz

	spin := SpinnerShow("Dowloading ")

	downloadTiming, err := DownloadFile(packageName, urlDownloadPackage, getAurtTempDir())

	spin.Stop()

	if err != nil {
		SpinnerErrorMessage("Downloading", err)
		RemoveFile(packageNameTarGz, getAurtTempDir())
		return 0
	}

	SpinnerCompletedMessage("Downloading", downloadTiming)

	return 1
}

// * Untar package from package name.
func untarPackage(packageName string) int {
	path := getAurtTempDir()

	spin := SpinnerShow("Untaring  ")

	untarTiming, err := UnTarGz(packageName, path, path)

	spin.Stop()

	if err != nil {
		SpinnerErrorMessage("Untaring   ", err)
		return 0
	}

	SpinnerCompletedMessage("Untaring   ", untarTiming)

	return 1
}

// * Build package from package name.
func buildPackage(packageName string) int {
	path := getAurtTempDir()

	spin := SpinnerShow("Building (it takes a long time.) ")

	buildingTiming, err := BuildFile(packageName, path)

	spin.Stop()

	if err != nil {
		SpinnerErrorMessage("Building   ", err)
		return 0
	}

	SpinnerCompletedMessage("Building   ", buildingTiming)

	return 1
}

// * Install package from package name.
func installPackage(packageName string) int {
	path := getAurtTempDir()

	spin := SpinnerShow("Installing  ")

	installingTiming, err := InstallFile(packageName, path)

	spin.NoTty = false

	spin.Stop()

	if err != nil {
		SpinnerErrorMessage("Installing ", err)
		return 0
	}

	SpinnerCompletedMessage("Installing ", installingTiming)

	return 1
}

func seachPackageFromFTP(packageName string) (string, int) {

	start := time.Now()

	spin := SpinnerShow("Searching (community) ")

	// find package from ftp
	packageData, err := SearchDataFromFTP(packageName)

	elapsed := time.Since(start)

	spin.Stop()

	if err != nil {
		SpinnerErrorMessage("Searching   ", err)
		return packageData, 0
	}
	if packageData == "" {
		SpinnerErrorMessage("Searching   ", errors.New("Missing"))
		return packageData, 0
	}

	SpinnerCompletedMessage("Searching  ", elapsed)

	return packageData, 1
}

func seachPackageFromSource(packageName string) int {

	start := time.Now()

	spin := SpinnerShow("Searching (pacman) ")

	packageNameTarGz := packageName + ".tar.gz"
	urlPackage := "https://" + urlSourcePackage + packageNameTarGz

	// find package from source.
	err := SearchFile(urlPackage)

	elapsed := time.Since(start)

	spin.Stop()

	if err != nil {
		SpinnerErrorMessage("Searching   ", errors.New("Missing"))
		return 0
	}

	SpinnerCompletedMessage("Searching   ", elapsed)

	return 1
}

// Print all summary packages process.
func ShowSummaryInstallPackage(packages int, commpletes int, fails int) {
	fmt.Println("")
	fmt.Printf("======> [ Packages  : %d ]\n", Bold(Blue(packages)))
	fmt.Printf("======> [ Completed : %d ]\n", Bold(Green(commpletes)))
	fmt.Printf("======> [ Failed    : %d ]\n", Bold(Red(fails)))
}

// Print all summary packages process.
func ShowSummarySearchPackage(packages int, found int, missing int) {
	fmt.Println("")
	fmt.Printf("======> [ Packages  : %d ]\n", Bold(Blue(packages)))
	fmt.Printf("======> [ Found     : %d ]\n", Bold(Green(found)))
	fmt.Printf("======> [ Missing   : %d ]\n", Bold(Red(missing)))
}
