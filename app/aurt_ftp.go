package app

import (
	"io"
	"os"
	"time"

	"aurt.sun/libs"
)

//* FTP
var ftpHostPackageFile = "ftp.lysator.liu.se"
var ftpPortPackageFile = "21"
var ftpUserPackageFile = "anonymous"
var ftpPassPackageFile = "anonymous"
var ftpPathPackageFile = "pub/manjaro/stable/community/x86_64/"

//* consts
const packageFileType = ".pkg.tar.xz"

var dirTmpPkgs = getAurtTempDir() + "/pkgs"

//* Set ftp for use package from ftp server.
func UseFTPPackageFile(host string, port string, username string, password string, pathPackage string) {
	if host != "" {
		ftpHostPackageFile = host
	}

	if port != "" {
		ftpPortPackageFile = port
	}

	if username != "" {
		ftpUserPackageFile = username
	}

	if password != "" {
		ftpPassPackageFile = password
	}

	if pathPackage != "" {
		ftpPathPackageFile = pathPackage
	}
}

//* Setup ftp and login
func setupAndConnectFTP() (*libs.FTP, error) {
	var ftpClient *libs.FTP

	ftpClient, err := libs.Connect(ftpHostPackageFile + ":" + ftpPortPackageFile)
	if err != nil {
		return nil, err
	}

	err = ftpClient.Login(ftpUserPackageFile, ftpPassPackageFile)
	if err != nil {
		return nil, err
	}

	return ftpClient, nil
}

//* Search package data from ftp server.
func SearchDataFromFTP(packageName string) (string, error) {

	// setup and login.
	ftpClient, err := setupAndConnectFTP()
	if err != nil {
		return "", err
	}

	// change directory to base package,
	err = ftpClient.Cwd(ftpPathPackageFile)
	if err != nil {
		return "", err
	}

	// search package by package name from ftp.
	packagesList, err := ftpClient.NList(packageName + "-*")

	packageData := ""
	if len(packagesList) > 0 {
		packageData = packagesList[0]
	}

	if err := ftpClient.Quit(); err != nil {
		return "", err
	}

	return packageData, nil
}

//* Download package file from ftp.
func DownloadPackageFromFTP(packageName string, packageDownloadName string) (time.Duration, string, string, error) {

	//setup and login.
	ftpClient, err := setupAndConnectFTP()
	if err != nil {
		return 0, "", "", err
	}

	//change directory to base package,
	err = ftpClient.Cwd(ftpPathPackageFile)
	if err != nil {
		return 0, "", "", err
	}

	// check aurt dir, if not has dir will create new aurt temp dir.
	if !checkHasDir(dirTmpPkgs) {
		err := makeDirAll(dirTmpPkgs)
		if err != nil {
			return 0, "", "", err
		}
	}

	start := time.Now()

	// create file for download from ftp.
	packageFileTar, err := os.Create(dirTmpPkgs + "/" + packageDownloadName + packageFileType)
	if err != nil {
		return 0, "", "", err
	}
	// close fo on exit and check for its returned error
	defer func() {
		packageFileTar.Close()
	}()

	// download package from ftp
	_, err = ftpClient.Retr(packageName, func(readPackageFile io.Reader) error {
		if _, err = io.Copy(packageFileTar, readPackageFile); err != nil {
			return err
		}

		return err
	})
	if err != nil {
		return 0, "", "", err
	}

	elapsed := time.Since(start)

	return elapsed, dirTmpPkgs, packageDownloadName + packageFileType, nil

}
