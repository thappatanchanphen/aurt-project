package app

import (
	"fmt"
	"time"

	"github.com/janeczku/go-spinner"
	. "github.com/logrusorgru/aurora"
)

var charsSet = []string{"⣾", "⣽", "⣻", "⢿", "⡿", "⣟", "⣯", "⣷"}

// * Public show spinner loading.
func SpinnerShow(messageLoading string) *spinner.Spinner {
	spinnerUse := spinner.NewSpinner(" ====> " + messageLoading)
	spinnerUse.SetCharset(charsSet)
	spinnerUse.Start()

	return spinnerUse
}

// * Print error loading
func SpinnerErrorMessage(messageLoading string, err error) {
	fmt.Printf("[ %s %s ] | %s message %s\n", Red("x"), messageLoading, Red("Failed"), err.Error())
}

// * Print completed loading
func SpinnerCompletedMessage(messageLoading string, timing time.Duration) {
	fmt.Printf("[ %s %s ] | %s in %s\n", Green("✓"), messageLoading, Green("Completed"), timing)
}
