# AURT PROJECT DETAILS
 - this project use `golang programing language` for develop.
 - this project for backup `aur` packages in your os (support only `arch` linux).
 - restore aur package from `.pkgs` files (download,build,install) on your linux.
 - install aur package from commandline input `-i 'package name'`
 - your must create file type `.pkgs` for keep packages name in this.

# SETUP ENV
 - file `.pkgs` in project, your has add package name in this.
 - file `.config` for setup config.  

# SHELL COMMAND FOR BUILD AND RUN AURT PROJECT
 - clone project `aurt project` 
    ````   
     url : git@gitlab.com:thappatanchanphen/aurt-project.git

    ````
 - build project `golang`
    ````
     cd aurt
     go build -o aurt .

    ````
 - run project
    ````
     #for show help.
      ./aurt -h

     #for install packages by file .pkgs .
      ./aurt -p '{password of user}'

     #for install package by commandline.
      ./aurt -i '{package name}' -p '{password of user}'

     #for search package.
      ./aurt -s '{package name}'

     #for search package by file .pkgs .
      ./aurt -s --a

    ````
# DOING LIST
 - Cretating function can search and install package with commandline. [```DONE```]
 - find package from url `https://ftp.lysator.liu.se/pub/manjaro/stable/community/x86_64/`, if has package use this package for installed. [```DONE```]
 - setup config file and load config to used. [```DONE```]
 - function search packages, this function for find package [```DONE```].

# TODO LIST 
 - function `backup package`, this function for backup packages in you os (auto backup) output to `.pkgs` file.
...   

# SHELL EXAMPLE FOR AUR INSTALL PACAKAGE
```
    #!/bin/sh

    mkdir -p /tmp/aur-build # Creates the directory if it doesn't exist
    cd /tmp/aur-build    

    pkg="$@ 
    wget "https://aur.archlinux.org/cgit/aur.git/snapshot/$pkg.tar.gz" # Download a snapshot of the package source files

    tar xvf "$pkg.tar.gz" # Decompress i    
    cd "$pkg"
    makepkg -si # -s stands for build, -i stands for install

 ```
