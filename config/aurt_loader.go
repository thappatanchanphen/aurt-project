package config

import (
	"os"

	"gopkg.in/yaml.v2"
)

type Configuration interface {
	filename() string
	instance() interface{}
}

func InitConfig(configpath string) error {
	if configpath == "" {
		configpath = "./"
	}

	err := load(configpath, AurtConfig)
	if err != nil {
		return err
	}

	return nil
}

func load(configpath string, config Configuration) error {
	conf := config.instance()
	return readYamlConfigFile(configpath, config.filename(), conf)
}

func readYamlConfigFile(configpath string, filename string, v interface{}) error {
	file, errFile := os.Open(configpath + filename)
	defer file.Close()
	if errFile != nil {
		return errFile
	}

	decoder := yaml.NewDecoder(file)
	errDecode := decoder.Decode(v)
	if errDecode != nil {
		return errDecode
	}

	return nil
}
