package config

type Config struct {
	AppDevmode bool      `yaml:"app_devmode"`
	AppConfig  AppConfig `yaml:"app_config"`
	PkgTarXz   PkgTarXz  `yaml:"pkg_tar_xz"`
}

type AppConfig struct {
	UseShell      string `yaml:"use_shell"`
	SourcePackage string `yaml:"source_pacakge"`
	PkgsFile      string `yaml:"pkgs_file"`
}

type PkgTarXz struct {
	PkgTarXzHost string `yaml:"pkg_tar_xz_host"`
	PkgTarXzPort string `yaml:"pkg_tar_xz_port"`
	PkgTarXzUser string `yaml:"pkg_tar_xz_user"`
	PkgTarXzPass string `yaml:"pkg_tar_xz_pass"`
	PkgTarXzPath string `yaml:"pkg_tar_xz_path"`
}

var AurtConfig *Config

func (c *Config) filename() string {
	return "config.yaml"
}

func (m *Config) instance() interface{} {
	AurtConfig = &Config{}
	return AurtConfig
}
